package com.example.mito.famicoco2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.mito.famicoco2.MainActivity.ServerUrl;

public class SOTOFragment extends Fragment{     //そとここ用のクラス
    @BindView(R.id.soto_listview) ListView soto_listView;

    private Timer mTimer = null;
    private Handler mHandler = null;
    private SimpleAdapter adapter;
//    private int[] iconid = new int[]{R.drawable.father, R.drawable.haruka, R.drawable.riku, R.drawable.grandfather, R.drawable.mother};
    private ArrayList<HashMap<String, Object>> list;
    private int iconId[];
    private JSONArray data;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_soto, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        setHasOptionsMenu(true);
        //そとここにセットするリスト用意
        list = new ArrayList<HashMap<String, Object>>();

        //準備
        adapter = new SimpleAdapter(getActivity(), list, R.layout.soto_row,
                new String[]{"iconkey0", "iconkey1", "iconkey2", "iconkey3"},
                new int[]{R.id.icon1, R.id.icon2, R.id.icon3, R.id.icon4});

        iconId = new int[]{R.drawable.haruka, R.drawable.riku, R.drawable.father, R.drawable.mother, R.drawable.grandfather};

        soto_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Intent intent = new Intent(getActivity(), SOTOTalkActivity.class);
                intent.putExtra("list", data.toString());
                intent.putExtra("select", pos);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new Handler();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // 実行したい処理

                        AsyncTask<URL, Void, JSONArray> task = new AsyncTask<URL, Void, JSONArray>(){
                            @Override
                            protected JSONArray doInBackground(URL... urls) {
                                HttpURLConnection con = null;
                                String readSt;
                                data = null;

                                try {
                                    final URL url = urls[0];
                                    con = (HttpURLConnection) url.openConnection();
                                    con.setDoInput(true);
                                    con.connect();
                                    InputStream in = con.getInputStream();
                                    readSt = HttpGetTask.readInputStream(in);
                                    JSONArray array = new JSONArray(readSt);
                                    data = array;
                                    list.clear();
//                                  int  m = array.length();
//                                    for (int i = 0; i < m; i++){
//                                        JSONArray data = array.getJSONArray(i);
//                                        int n = data.length();
//                                        Log.d("json", data.toString());
//                                        HashMap<String, Object> item = new HashMap<String, Object>();
//                                        item.clear();
//                                        for (int j = 0; j < n; j++){
//                                            Bitmap bitmap;
//                                            int num;
//                                            String usr = data.getString(j);
//                                            Log.d("user", usr);
////                                            if(usr.equals("しゅり")) bitmap = icon[0];
////                                            else if (usr.equals("しゅうき")) bitmap = icon[1];
////                                            else if (usr.equals("お母さん")) bitmap = icon[2];
////                                            else if (usr.equals("お父さん")) bitmap = icon[3];
////                                            else bitmap = icon[4];
//                                            if (usr.equals("しゅり")) num = iconId[0];
//                                            else if (usr.equals("しゅうき")) num = iconId[1];
//                                            else if (usr.equals("お父さん")) num = iconId[2];
//                                            else if (usr.equals("お母さん")) num = iconId[3];
//                                            else num = iconId[4];
//                                            item.put("iconkey" + i, num);
//                                            Log.d("soto", "item_put");
//                                        }
//                                        list.add(item);
//                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return data;
                            }

                            @Override
                            protected void onPostExecute(JSONArray array) {
                                int m = 0;
                                if (array != null)
                                m = array.length();
                                for (int i = 0; i < m; i++){
                                    try {
                                        JSONArray data = array.getJSONArray(i);
                                        int n = data.length();
                                        HashMap<String, Object> item = new HashMap<String, Object>();
                                        for (int j = 0; j < n; j++){
                                            int num;
                                            String usr = data.getString(j);
                                            if (usr.equals("しゅり")) num = iconId[0];
                                            else if (usr.equals("しゅうき")) num = iconId[1];
                                            else if (usr.equals("お父さん")) num = iconId[2];
                                            else if (usr.equals("お母さん")) num = iconId[3];
                                            else num = iconId[4];
                                            item.put(String.valueOf("iconkey" + j), num);
                                        }
                                        list.add(item);
                                        soto_listView.setAdapter(adapter);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        };
                        try {
                            URL url = new URL(ServerUrl + "get_out/");
                            task.execute(url);
                        } catch (MalformedURLException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 5000, 5000); // 実行したい間隔(ミリ秒)
    }

    public SOTOFragment() {
    }
}