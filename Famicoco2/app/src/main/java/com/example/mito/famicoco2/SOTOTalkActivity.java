package com.example.mito.famicoco2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static com.example.mito.famicoco2.MainActivity.ServerUrl;
import static com.example.mito.famicoco2.MainActivity.myName;

public class SOTOTalkActivity extends AppCompatActivity implements CustomListView.OnKeyboardAppearedListener { //そとここのリストを押したときに呼ばれるトーク画面用Activity

    @BindView(R.id.soto_talk_listview) CustomListView listView;
    @BindView(R.id.button) Button btn;
    @BindView(R.id.soto_editText) EditText editText;
    @BindView(R.id.ie_imageView)  ImageView imageView1;
    @BindView(R.id.ie_imageView2) ImageView imageView2;
    @BindView(R.id.ie_imageView3) ImageView imageView3;
    @BindView(R.id.ie_imageView4) ImageView imageView4;
    @BindView(R.id.ie_imageView5) ImageView imageView5;

    private Timer mTimer = null;
    private Handler mHandler = null;
    private ArrayList<CustomData> list;
    private TalkCustomAdapter adapter;
    private ArrayList<Object> soto_list = new ArrayList<Object>();
    private int pos = 0;
    private String data;
    private JSONArray array, select;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sototalk);
        ButterKnife.bind(this);
        setTitle("そとここ");

        Intent i = getIntent();
        data = i.getStringExtra("list");
        pos = i.getIntExtra("select", 0);

        list = new ArrayList<>();
        listView.setListener(this);

        try {
            array = new JSONArray(data);
            select = array.getJSONArray(pos);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //戻るボタン表示
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setHomeButtonEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);

        adapter = new TalkCustomAdapter(this, 0, list);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String str = editText.getText().toString();
                CustomData item = new CustomData();
                if (!str.equals("")){
                    try {
                        str = URLEncoder.encode(str, "UTF-8");
                        String name = URLEncoder.encode(myName, "UTF-8");
                        URL url = new URL(ServerUrl + "soto/talk/?name=" + name + "&text=" + str);
                        new HttpGetTask().execute(url);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    item.setComment(str);
                    item.setName(myName);
                    item.setTime();
                    list.add(item);
                    editText.setText("");
                    listView.setAdapter(adapter);
                }
                KeyboardUtils.hide(getApplicationContext(), view);
            }
        });

    }

    @Override
    public void onKeyboardAppeared(boolean isChange) {

        //ListView生成済、且つサイズ変更した（キーボードが出現した）場合
        if(isChange){

            //リストアイテムの総数-1（0番目から始まって最後のアイテム）にスクロールさせる
            Handler handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    //リストアイテムの総数-1（0番目から始まって最後のアイテム）にフォーカスさせる
                    listView.smoothScrollToPosition(listView.getCount()-1);
                }
            };
            handler.postDelayed(runnable, 500);

            //スクロールアニメーションが要らない場合はこれでOK
            //listView.setSelection(listView.getCount()-1);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler = new Handler();
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // 実行したい処理
                        AsyncTask<URL, Void, ArrayList<UpdateItem>> task = new AsyncTask<URL, Void, ArrayList<UpdateItem>>() {
                            @Override
                            protected ArrayList<UpdateItem> doInBackground(URL... urls) {
                                final URL url = urls[0];
                                HttpURLConnection con;
                                String readSt;

                                OkHttpClient client = new OkHttpClient();

                                RequestBody body = new FormBody.Builder()
                                        .add("data", select.toString())
                                        .build();
                                Request request = new Request.Builder()
                                        .url(url)
                                        .post(body)
                                        .build();

                                try {
                                    client.newCall(request).execute();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                UpdateItem updateItem;
                                ArrayList<UpdateItem> updateItemList = new ArrayList<>();
                                try {

                                    con = (HttpURLConnection) url.openConnection();
                                    con.setDoInput(true);
                                    con.connect();
                                    InputStream in = con.getInputStream();
                                    readSt = HttpGetTask.readInputStream(in);
                                    JSONArray data = new JSONArray(readSt);
                                    JSONArray member = data.getJSONArray(0);
                                    soto_list.clear();
                                    int m = member.length();
                                    for (int i = 0; i < m; i++){
                                        soto_list.add(member.get(i));
                                    }
                                    JSONArray talks = data.getJSONArray(1);
                                    m = talks.length();
                                    for (int i = 0; i < m; i++) {
                                        JSONObject talk = talks.getJSONObject(i);
                                        updateItem = new UpdateItem(talk);
                                        updateItemList.add(updateItem);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return updateItemList;
                            }

                            @Override
                            protected void onPostExecute(ArrayList<UpdateItem> updateItemArrayList) {
                                int m = soto_list.size();
                                imageView1.setImageResource(R.color.SubColor);
                                imageView2.setImageResource(R.color.SubColor);
                                imageView3.setImageResource(R.color.SubColor);
                                imageView4.setImageResource(R.color.SubColor);
                                imageView5.setImageResource(R.color.SubColor);
                                for (int i = 0; i < m; i++){
                                    if (i == 0) imageView1.setImageBitmap(IEFragment.judge(soto_list.get(0)));
                                    else if (i == 1) imageView2.setImageBitmap(IEFragment.judge(soto_list.get(1)));
                                    else if (i == 2) imageView3.setImageBitmap(IEFragment.judge(soto_list.get(2)));
                                    else if (i == 3) imageView4.setImageBitmap(IEFragment.judge(soto_list.get(3)));
                                    else imageView5.setImageBitmap(IEFragment.judge(soto_list.get(4)));
                                }
                                if (list.size() != updateItemArrayList.size()) {
                                    list.clear();
                                    adapter = new TalkCustomAdapter(getApplicationContext(), 0, list);
                                    m = updateItemArrayList.size();
                                    for (int i = 0; i < m; i++) {
                                        UpdateItem updateItem = updateItemArrayList.get(i);
                                        list.add(0, updateItem.toCustomData());
                                        listView.setAdapter(adapter);
                                    }
                                    onKeyboardAppeared(true);
                                }
                            }
                        };
                        try {
                            URL url = new URL(ServerUrl + "home/");
                            task.execute(url);

                        } catch (MalformedURLException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, 5000, 5000); // 実行したい間隔(ミリ秒)
    }

    // ActionBarの「<」を押した時に元の画面に戻るように
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }
}
