package com.example.mito.famicoco2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.mito.famicoco2.MainActivity.myName;

/**
 * Created by mito on 2016/10/16.
 */

public class TalkCustomAdapter extends ArrayAdapter<CustomData>{
    private LayoutInflater layoutInflater;


    @Override
    public int getViewTypeCount() {
        return 2;
    }


    public TalkCustomAdapter(Context c, int id, ArrayList<CustomData> list){
        super(c, id, list);
        this.layoutInflater = (LayoutInflater) c.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        CustomData customData = (CustomData) getItem(position);
        if (!customData.getName().equals(myName)) {
                view = layoutInflater.inflate(R.layout.ie_row2, parent, false);
        }
        else {
                view = layoutInflater.inflate(R.layout.ie_row, parent, false);
        }
        ((ImageView) view.findViewById(R.id.ie2_icon))
                .setImageBitmap(customData.getIcon());
        ((TextView) view.findViewById(R.id.ie2_textView))
                .setText(customData.getName());
        ((TextView) view.findViewById(R.id.ie2_textView2))
                .setText(customData.getComment());
        ((TextView)view.findViewById(R.id.time_now))
                .setText(customData.getTime_now());
        return view;
    }

    //タッチ無効
    public boolean isEnabled(int position) {
        return false;
    }
}
