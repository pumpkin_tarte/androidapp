package com.example.mito.famicoco2;

import android.graphics.Bitmap;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by mito on 2016/09/01.
 */
public class CustomData {       //カスタムデータをセットするためのクラス
    private Bitmap icon;
    private String name;
    private String comment;
    private String time_now;

    public void setTime(String time) {
        this.time_now = time;
    }
    public void setTime(){
        final DateFormat df = new SimpleDateFormat("HH:mm");
        final Date date = new Date(System.currentTimeMillis());
        this.time_now = df.format(date);
    }

    public String getTime_now() {
        return time_now;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

    public Bitmap getIcon() {
        return icon;
    }


}

