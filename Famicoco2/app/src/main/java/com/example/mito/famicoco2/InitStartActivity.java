package com.example.mito.famicoco2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.mito.famicoco2.MainActivity.ServerUrl;
import static com.example.mito.famicoco2.MainActivity.myBeaconMacAddress;
import static com.example.mito.famicoco2.MainActivity.myName;
import static com.example.mito.famicoco2.MainActivity.myRegistarationId;

public class InitStartActivity extends AppCompatActivity {
    @BindView(R.id.name_init_setting) EditText editText;
    @BindView(R.id.button_setting) Button btn;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_icon_setting);
        ButterKnife.bind(this);
        setTitle("ふぁみここ");

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editText.getText().toString();
                if (name != "") {
                    myName = name;
                    Log.d("hoge", myBeaconMacAddress);
                    String str;
                    str = myBeaconMacAddress.substring(0, 2) + myBeaconMacAddress.substring(myBeaconMacAddress.length() - 2);
                    Log.d("hoge", str);
                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    sp.edit().putString("name", name).commit();
                    sp.edit().putString("myBeacon", str).commit();

                    try {
                        name = URLEncoder.encode(name, "UTF-8");
                        String urlString = ServerUrl + "init_famicoco/" + "?user_name=" + name + "&beacon_id=" + str + "&regi_id=" + myRegistarationId;
                        URL url = new URL(urlString);
                        new HttpGetTask().execute(url);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    finish();
                }
            }
        });
    }
}