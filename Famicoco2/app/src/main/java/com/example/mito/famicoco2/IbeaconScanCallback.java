package com.example.mito.famicoco2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import static com.example.mito.famicoco2.MainActivity.ServerUrl;
import static com.example.mito.famicoco2.MainActivity.myBeaconMacAddress;
import static com.google.android.gms.internal.zzs.TAG;

/**
 * Created by mito on 2016/09/13.
 */
public class IbeaconScanCallback extends ScanCallback{      //beaconを検知した時に呼ばれる

    String tag = new String();  //tag 適当な文字列
    Boolean f = true;
    ArrayList<String> beaconId = new ArrayList<>();
    Boolean f_init = true;

    @Override
    public void onBatchScanResults(List<ScanResult> results) {
        super.onBatchScanResults(results);
        // BLE受信のディレイ（ScanSettings$Builder#setReportDelay）を設定すると
        // こちらが呼び出されました
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        super.onScanResult(callbackType, result);
        // スキャン結果が返ってきます
        // このメソッドかonBatchScanResultsのいずれかが呼び出されます。
        // 通常はこちらが呼び出されます。
        // beaconを検知したらここが呼ばれるみたいだからこの後に処理書けば良さげ
        BluetoothDevice bluetoothDevice = result.getDevice();
        if (bluetoothDevice.getName() == null) {
            tag = "AC";
            Log.d(tag, "beacon");
            Log.d(tag, "address:" + bluetoothDevice.getAddress());
            Log.d(tag, "name:" + bluetoothDevice.getName());
            if (f_init) {
                f_init = false;
                myBeaconMacAddress = bluetoothDevice.getAddress();
            }
            String str = bluetoothDevice.getAddress();
            str = str.substring(0, 2) + str.substring(str.length() - 2);
            Log.d("BeaconId", str);
            beaconId.add(str);
            if (f) {
                f = false;
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // ここにn秒後に実行したい処理
                        SendBeaconId();
                        f = true;
//                        beaconId.clear();
                    }
                }, 10000);
            }
        }
    }

    public void SendBeaconId(){
        AsyncTask<URL, Void, Boolean> asyncTask = new AsyncTask<URL, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(URL... urls) {
                final URL url = urls[0];

                OutputStream os = null;
                HttpURLConnection con = null;
                OkHttpClient client = new OkHttpClient();
                JSONArray array = new JSONArray();
                for (int i = 0; i < beaconId.size(); i++) {
                    array.put(beaconId.get(i));
                }

                RequestBody body = new FormBody.Builder()
                        .add("data", array.toString())
                        .build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(body)
                        .build();

                try {
                    client.newCall(request).execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                beaconId.clear();
                return true;
            }
        };

        try {
            URL url = new URL(ServerUrl + "receive_beacon/");
            asyncTask.execute(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /** F8:AC:78:96:28:6A
     * {@inheritDoc}
     */
    @Override
    public void onScanFailed(int errorCode) {
        super.onScanFailed(errorCode);
        // エラーが発生するとこちらが呼び出されます
        String errorMessage = "";
        switch (errorCode) {
            case SCAN_FAILED_ALREADY_STARTED:
                errorMessage = "既にBLEスキャンを実行中です";
                break;
            case SCAN_FAILED_APPLICATION_REGISTRATION_FAILED:
                errorMessage = "BLEスキャンを開始できませんでした";
                break;
            case SCAN_FAILED_FEATURE_UNSUPPORTED:
                errorMessage = "BLEの検索をサポートしていません。";
                break;
            case SCAN_FAILED_INTERNAL_ERROR:
                errorMessage = "内部エラーが発生しました";
                break;
        }
        Log.d(TAG, errorMessage);
    }
}
