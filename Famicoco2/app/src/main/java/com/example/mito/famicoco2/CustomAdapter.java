package com.example.mito.famicoco2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mito on 2016/09/02.
 */
public class CustomAdapter extends ArrayAdapter<CustomData> {  //リストに表示するためのadapterを用意するクラス
    private LayoutInflater layoutInflater;

    public CustomAdapter(Context c, int id, ArrayList<CustomData> list){
        super(c, id, list);
        this.layoutInflater = (LayoutInflater) c.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if (convertView == null){
            convertView = layoutInflater.inflate(R.layout.tl_row, parent, false);
        }

        CustomData customData = (CustomData)getItem(position);
        ((ImageView) convertView.findViewById(R.id.icon))
                .setImageBitmap(customData.getIcon());
        ((TextView) convertView.findViewById(R.id.time_now))
                .setText(customData.getTime_now());
        ((TextView) convertView.findViewById(R.id.tl_text))
                .setText(customData.getComment());
        return convertView;
    }
    //タッチ無効
    public boolean isEnabled(int position) {
        return false;
    }
}