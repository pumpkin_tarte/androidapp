package com.example.mito.famicoco2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.mito.famicoco2.MainActivity.ServerUrl;

public class PETFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private CustomAdapter adapter;
    private ArrayList<CustomData> list;

    @BindView(R.id.pet_listview) ListView listView;
    @BindView(R.id.swipe_pet) SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pet, container, false);
        ButterKnife.bind(this, v);

        swipeRefreshLayout.setColorSchemeResources(R.color.MainColorDark,
                R.color.MainColor, R.color.MainColorDark, R.color.MainColor);
        swipeRefreshLayout.setOnRefreshListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        list = new ArrayList<>();
        adapter = new CustomAdapter(getContext(), 0, list);
    }

    public PETFragment() {
    }

    @Override
    public void onResume() {
        UpData();
        super.onResume();
    }

    public void UpData() {
        AsyncTask<URL, Void, ArrayList<TLUpdateItem>> task = new AsyncTask<URL, Void, ArrayList<TLUpdateItem>>() {
            @Override
            protected ArrayList<TLUpdateItem> doInBackground(URL... values) {
                HttpURLConnection con = null;
                TLUpdateItem item;
                ArrayList<TLUpdateItem> tlUpdateItems = new ArrayList<>();
                try {
                    // アクセス先URL
                    final URL url = values[0];
                    // ローカル処理
                    // コネクション取得
                    con = (HttpURLConnection) url.openConnection();
                    con.setDoInput(true);
                    con.connect();
                    InputStream in = con.getInputStream();
                    String readSt = HttpGetTask.readInputStream(in);

                    //// 配列を取得する場合
                    JSONArray jsonArray = new JSONArray(readSt);

                    int m = jsonArray.length();
                    for (int i = 0; i < m; i++) {
                        JSONObject data = jsonArray.getJSONObject(i);
                        item = new TLUpdateItem(data);
                        tlUpdateItems.add(item);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (con != null) {
                        // コネクションを切断
                        con.disconnect();
                    }
                }
                return tlUpdateItems;
            }

            @Override
            protected void onPostExecute(ArrayList<TLUpdateItem> tlUpdateItemArrayList) {
                list.clear();
                adapter = new CustomAdapter(getContext(), 0, list);
                int m = tlUpdateItemArrayList.size();
                for (int i = 0; i < m; i++) {
                    TLUpdateItem tlUpdateItem = tlUpdateItemArrayList.get(i);
                    list.add(tlUpdateItem.toCustomData());
                    listView.setAdapter(adapter);
                }
                Log.d("hoge", "here");
            }
        };
        try {
            URL url = new URL(ServerUrl + "get_tl/");
            task.execute(url);
        } catch (MalformedURLException e){
            e.printStackTrace();
        }
    }
    @Override
    public void onRefresh() {
        UpData();
        swipeRefreshLayout.setRefreshing(false);
    }
}
